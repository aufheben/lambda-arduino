{-# LANGUAGE PackageImports #-}
import "arduino" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
